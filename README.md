#	Dynamic Elasticube Plugin

This plugin was created to decide on the fly, which Elasticube should be used to power a given dashboard.  The logic here is that you define a security object which is a dictionary of dashboard titles.  Each dashboard object contains a dictionary with keys for different user groups IDs, along with the proper Elasticube (name) to load for that group.

```
prism.dynamicElasticubeSettings = {
	security: {
		"MyDashboardTitle" : {
			"tenantA-groupId": "tenantA-elasticubeName",
			"tenantB-groupId": "tenantB-elasticubeName"
		}
	}
};
```

An important note is that this implementation is just an example of how the business logic can be implemented.  All business logic for deciding what Elasticube should be loaded is contained in dynamicElasticubes.js.  The settings.js file is just a configuration file, where any settings for this plugin are stored.

__Step 1: Configure settings.js__ - Open this file with a text editor and add your list of dashboards, following the above syntax.  

__Step 2: Copy the plugin__ - Copy the dynamicaElasticube folder into your C:\program files\sisense\PrismWeb\plugins directory.

__Step 3: Add Security__ - This plugin simply redirects queries to one elasticube or another.  It is **critical** to also apply Elasticube level security based on these user groups, so that if anything goes wrong (maybe a new dashboard gets added but not included in the settings.js file), you don't accidentally show data from tenant-a to tenant-b.  In order to setup Elasticube level security, please see the documentation here

https://documentation.sisense.com/security/#Assign_rights_EC

This sample has been confirmed working on Sisense version 6.7.1, and should be backwards compatible with previous version.
