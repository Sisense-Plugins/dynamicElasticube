
prism.run(["$jaql", function ($jaql) {	

	function elasticubeInfoHandler(datasource){

		//	Update the dashboard
		prism.activeDashboard.datasource = datasource;

		//	Update the widgets
		$.each(prism.activeDashboard.widgets.$$widgets, function(){
			this.datasource = datasource;
		});

		console.log("Security: Elasticube Switched");
	}	

	// uses to intercept all widget queries of a given dashboard and apply dynamic values into predefined placeholders 
	function elasticubeSwitcher(dashboard) {
		
        if (!defined(prism,'user.groups')) {
        	return;
        }
		//	Get the list of user groups for this Dashboard
		var dashboardSecurity = $$get(prism.dynamicElasticubeSettings.security, dashboard.title, {});

		//	Look for a matching group id, for this elasticube
		var securityGroups = $.grep(prism.user.groups,function(w){
			return dashboardSecurity[w];
		});

		//make sure security is defined
        if (!securityGroups || securityGroups.length === 0) {
        	return;
        }
			
		//	Figure out the new Elasticube 
		var newElasticube = dashboardSecurity[securityGroups[0]];

        //no need to change if ECs are the same - for example when export to PDF
        if (newElasticube === dashboard.datasource.title) {
        	return;
        }
		//	Figure out the API call url
		var elasticubeInfoUrl = '/api/elasticubes/metadata/' + newElasticube;

		//	Make the API calls
		$.ajax({
			async:false,
			url: elasticubeInfoUrl,
			method: 'GET'
		}).done(elasticubeInfoHandler);
	}

	// Run the Elasticube Switcher on load
	prism.on("dashboardloaded", function (e, args) {		
		elasticubeSwitcher(args.dashboard);
	});

}]);


